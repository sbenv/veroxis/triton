use chrono::prelude::*;
use chrono::Duration;
use color_eyre::eyre::ContextCompat;
use color_eyre::Result;
use reqwest::StatusCode;
use serde_json::Value;
use triton_gitlab::raw::events::build::Build;
use triton_gitlab::raw::events::deployment::Deployment;
use triton_gitlab::raw::events::feature_flag::FeatureFlag;
use triton_gitlab::raw::events::issue::Issue;
use triton_gitlab::raw::events::merge_request::MergeRequest;
use triton_gitlab::raw::events::note::Note;
use triton_gitlab::raw::events::pipeline::Pipeline;
use triton_gitlab::raw::events::push::Push;
use triton_gitlab::raw::events::release::Release;
use triton_gitlab::raw::events::wiki_page::WikiPage;

use crate::CliArgs;

#[derive(Debug, Clone)]
pub struct RequestArgs {
    url: String,
    secret: Option<String>,
    max_age: i64,
}

impl From<&CliArgs> for RequestArgs {
    fn from(cli_args: &CliArgs) -> Self {
        Self {
            url: cli_args.triton_url.clone(),
            secret: cli_args.triton_api_secret.clone(),
            max_age: cli_args.max_age,
        }
    }
}

pub async fn query_builds(request_args: RequestArgs) -> Result<Vec<Build>> {
    let response = query_triton_by_kind(request_args, "build").await?;
    let builds = serde_json::from_value::<Vec<Build>>(response)?;
    Ok(builds)
}

pub async fn query_deployments(request_args: RequestArgs) -> Result<Vec<Deployment>> {
    let response = query_triton_by_kind(request_args, "deployment").await?;
    let deployments = serde_json::from_value::<Vec<Deployment>>(response)?;
    Ok(deployments)
}

pub async fn query_feature_flags(request_args: RequestArgs) -> Result<Vec<FeatureFlag>> {
    let response = query_triton_by_kind(request_args, "feature_flag").await?;
    let feature_flags = serde_json::from_value::<Vec<FeatureFlag>>(response)?;
    Ok(feature_flags)
}

pub async fn query_issues(request_args: RequestArgs) -> Result<Vec<Issue>> {
    let response = query_triton_by_kind(request_args, "issue").await?;
    let issues = serde_json::from_value::<Vec<Issue>>(response)?;
    Ok(issues)
}

pub async fn query_merge_requests(request_args: RequestArgs) -> Result<Vec<MergeRequest>> {
    let response = query_triton_by_kind(request_args, "merge_request").await?;
    let merge_requests = serde_json::from_value::<Vec<MergeRequest>>(response)?;
    Ok(merge_requests)
}

pub async fn query_notes(request_args: RequestArgs) -> Result<Vec<Note>> {
    let response = query_triton_by_kind(request_args, "note").await?;
    let notes = serde_json::from_value::<Vec<Note>>(response)?;
    Ok(notes)
}

pub async fn query_pipelines(request_args: RequestArgs) -> Result<Vec<Pipeline>> {
    let response = query_triton_by_kind(request_args, "pipeline").await?;
    let pipelines = serde_json::from_value::<Vec<Pipeline>>(response)?;
    Ok(pipelines)
}

pub async fn query_pushs(request_args: RequestArgs) -> Result<Vec<Push>> {
    let response = query_triton_by_kind(request_args, "push").await?;
    let pushs = serde_json::from_value::<Vec<Push>>(response)?;
    Ok(pushs)
}

pub async fn query_releases(request_args: RequestArgs) -> Result<Vec<Release>> {
    let response = query_triton_by_kind(request_args, "release").await?;
    let releases = serde_json::from_value::<Vec<Release>>(response)?;
    Ok(releases)
}

pub async fn query_wiki_pages(request_args: RequestArgs) -> Result<Vec<WikiPage>> {
    let response = query_triton_by_kind(request_args, "wiki_page").await?;
    let wiki_pages = serde_json::from_value::<Vec<WikiPage>>(response)?;
    Ok(wiki_pages)
}

async fn query_triton_by_kind(request_args: RequestArgs, object_kind: &str) -> Result<Value> {
    let today: DateTime<Utc> = Utc::now();
    let age_duration: Duration = Duration::try_days(request_args.max_age)
        .context("cant convert request_args.max_age to days")?;
    let mut oldest_date: DateTime<Utc> = today - age_duration;
    // safety: can be unwrapped because 0 is guaranteed to be a valid value
    oldest_date = oldest_date.with_hour(0).unwrap();
    oldest_date = oldest_date.with_minute(0).unwrap();
    oldest_date = oldest_date.with_second(0).unwrap();
    let mut query_args = vec![format!(
        "min_date={}",
        oldest_date.to_rfc3339_opts(SecondsFormat::Secs, true)
    )];

    if let Some(secret) = request_args.secret {
        query_args.push(format!("secret={secret}"));
    }

    let url = format!(
        "{url}/api/webhook_events_by_kind?{query_args}&object_kind={object_kind}",
        url = request_args.url,
        query_args = query_args.join("&")
    );
    let response = reqwest::Client::builder().build()?.get(url).send().await?;

    if response.status() != StatusCode::OK {
        return Err(color_eyre::eyre::eyre!(
            "ResponseCodeNotOk: {}",
            response.status()
        ));
    }

    Ok(response.json::<Value>().await?)
}
