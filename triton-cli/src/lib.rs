use clap::Parser;
use color_eyre::Result;
use commands::active_pipelines::ActivePipelineArgs;
use commands::{active_pipelines, raw};

pub mod commands;
pub mod request;

#[derive(Debug, Parser, Clone)]
pub struct CliArgs {
    /// triton server url to query, e.g.: `https://triton.example.com`
    #[clap(long)]
    pub triton_url: String,

    /// the secret to pass to triton if triton is configured to require a secret
    #[clap(long)]
    pub triton_api_secret: Option<String>,

    /// max days into the past until which to load webhook events
    #[clap(long, default_value = "1")]
    pub max_age: i64,

    #[clap(subcommand)]
    pub command: Command,
}

#[derive(Parser, Debug, Clone)]
#[clap(name = "command", about)]
pub enum Command {
    /// view which pipelines are currently running
    #[clap(name = "active_pipelines")]
    ActivePipelines(ActivePipelineArgs),

    /// query all `build` webhooks
    #[clap(name = "builds")]
    Builds,

    /// query all `deployment` webhooks
    #[clap(name = "deployments")]
    Deployments,

    /// query all `feature_flag` webhooks
    #[clap(name = "feature_flags")]
    FeatureFlags,

    /// query all `issue` webhooks
    #[clap(name = "issues")]
    Issues,

    /// query all `merge_request` webhooks
    #[clap(name = "merge_requests")]
    MergeRequests,

    /// query all `note` webhooks
    #[clap(name = "notes")]
    Notes,

    /// query all `pipeline` webhooks
    #[clap(name = "pipelines")]
    Pipelines,

    /// query all `push` webhooks
    #[clap(name = "pushs")]
    Pushs,

    /// query all `release` webhooks
    #[clap(name = "releases")]
    Releases,

    /// query all `wiki_page` webhooks
    #[clap(name = "wiki_pages")]
    WikiPages,
}

pub async fn run(cli_args: CliArgs) -> Result<()> {
    match &cli_args.command {
        Command::ActivePipelines(active_pipeline_args) => {
            active_pipelines::active_pipelines(&cli_args, active_pipeline_args.clone()).await?
        }
        Command::Builds => raw::builds(&cli_args).await?,
        Command::Deployments => raw::deployments(&cli_args).await?,
        Command::FeatureFlags => raw::feature_flags(&cli_args).await?,
        Command::Issues => raw::issues(&cli_args).await?,
        Command::MergeRequests => raw::merge_request(&cli_args).await?,
        Command::Notes => raw::notes(&cli_args).await?,
        Command::Pipelines => raw::pipelines(&cli_args).await?,
        Command::Pushs => raw::pushs(&cli_args).await?,
        Command::Releases => raw::releases(&cli_args).await?,
        Command::WikiPages => raw::wiki_pages(&cli_args).await?,
    }
    Ok(())
}
