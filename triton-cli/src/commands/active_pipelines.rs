use std::collections::BTreeMap;
use std::time::Duration;

use chrono::Utc;
use clap::Parser;
use color_eyre::eyre::ContextCompat;
use color_eyre::Result;
use futures_util::{SinkExt, StreamExt};
use serde_json::{json, Value};
use tokio::sync::broadcast;
use tokio::time::sleep;
use tokio_tungstenite::connect_async;
use tokio_tungstenite::tungstenite::Message;
use triton_gitlab::entities::job::{Job, JobState};
use triton_gitlab::entities::project_store::ProjectStore;
use triton_gitlab::webhook::Webhook;

use crate::request::{query_builds, query_pipelines};
use crate::CliArgs;

#[derive(Debug, Parser, Clone)]
pub struct ActivePipelineArgs {
    /// keep updating
    #[clap(short, long)]
    pub watch: bool,
}

pub async fn active_pipelines(
    cli_args: &CliArgs,
    active_pipeline_args: ActivePipelineArgs,
) -> Result<()> {
    if active_pipeline_args.watch {
        run_active_pipeline_dashboard(cli_args).await?;
    } else {
        let mut project_store = ProjectStore::new();
        load_data(&mut project_store, cli_args).await?;
        print_statistic(&project_store, cli_args).await?;
    }
    Ok(())
}

async fn run_active_pipeline_dashboard(cli_args: &CliArgs) -> Result<()> {
    let (webhook_tx, mut webhook_rx) = broadcast::channel::<Value>(255);
    let cli_args_websocket = (*cli_args).clone();

    // the url is assumed to start with http or https
    if !&cli_args_websocket.triton_url.starts_with("http") {
        return Err(color_eyre::eyre::eyre!(
            "TritonUrlDoesNotStartWithHttpOrHttps"
        ));
    }

    // this cli app will be closed by ctrl+c or other signals
    //
    // since it doesnt have an integrated way to be closed, the handle is ignored forever
    let _webhook_task = tokio::spawn(async move {
        // `http` and `https` have to be converted to `ws` and `wss` respectively
        //
        // since the `s` at the end is there automatically we only
        // need to replace `http` with `ws` and the "secure" version will be
        // there automatically
        let mut triton_webhook_url = cli_args_websocket.triton_url.replacen("http", "ws", 1);
        triton_webhook_url = format!("{triton_webhook_url}/api/socket");
        if let Some(secret) = cli_args_websocket.triton_api_secret {
            triton_webhook_url = format!("{triton_webhook_url}?secret={secret}");
        }

        match url::Url::parse(&triton_webhook_url) {
            Ok(url) => {
                match connect_async(url).await {
                    Ok((mut socket, _)) => {
                        loop {
                            tokio::select! {
                                Some(msg) = socket.next() => {
                                    match msg {
                                        Ok(message) => {
                                            match message {
                                                Message::Text(websocket_data) => {
                                                    match serde_json::from_str::<Value>(websocket_data.as_str()) {
                                                        Ok(websocket_data) => {
                                                            if let Err(error) = webhook_tx.send(websocket_data) {
                                                                tracing::error!("failed to send webhook: {error}");
                                                            }
                                                        },
                                                        Err(error) => tracing::error!("failed to serialize received data: {error}"),
                                                    };
                                                },
                                                Message::Binary(_) => {},
                                                Message::Ping(_) => {},
                                                Message::Pong(_) => {},
                                                Message::Close(_) => {},
                                                Message::Frame(_) => {},
                                            }
                                        },
                                        Err(error) => {
                                            tracing::info!("error: {error}");
                                        },
                                    }
                                },
                                // sending a ping every 15 seconds if nothing happens to keep connection alive
                                _ = sleep(Duration::from_secs(15)) => {
                                    if let Err(error) = socket.send(Message::Ping(vec![])).await {
                                        tracing::error!("failed to send ping: {error}");
                                    }
                                }
                            };
                        }
                    }
                    Err(error) => tracing::error!("failed to connect to triton socket: {error}"),
                };
            }
            Err(error) => tracing::error!("failed to parse webhook url: {error}"),
        };
    });

    let mut project_store = ProjectStore::new();
    let term = console::Term::stdout();
    load_data(&mut project_store, cli_args).await?;
    term.clear_screen()?;
    print_statistic(&project_store, cli_args).await?;
    loop {
        let event_data = webhook_rx.recv().await;
        match event_data {
            Ok(webhook_value) => {
                let webhook_parse_result = Webhook::try_from(&webhook_value);
                match webhook_parse_result {
                    Ok(webhook) => match &webhook {
                        Webhook::Build(_) => {
                            project_store.push_event(webhook);
                        }
                        Webhook::Deployment(_) => {}
                        Webhook::FeatureFlag(_) => {}
                        Webhook::Issue(_) => {}
                        Webhook::MergeRequest(_) => {}
                        Webhook::Note(_) => {}
                        Webhook::Pipeline(_) => {
                            project_store.push_event(webhook);
                        }
                        Webhook::Push(_) => {}
                        Webhook::Release(_) => {}
                        Webhook::TagPush(_) => {}
                        Webhook::WikiPage(_) => {}
                    },
                    Err(_error) => {}
                }
            }
            Err(receiver_error) => {
                tracing::error!("failed to receive event_data: {}", receiver_error);
            }
        }
        term.clear_screen()?;
        print_statistic(&project_store, cli_args).await?;
    }
}

async fn print_statistic(project_store: &ProjectStore, cli_args: &CliArgs) -> Result<()> {
    let max_age = chrono::Duration::try_days(cli_args.max_age)
        .context("cant convert cli_args.max_age to days")?;
    let now = Utc::now();
    let oldest_allowed_date = now - max_age;

    println!(
        "Time Range: {} -> {}",
        oldest_allowed_date.format("%Y-%m-%d"),
        now.format("%Y-%m-%d")
    );
    println!("Last Update: {}", now.format("%Y-%m-%d %H:%M:%S"));
    println!();

    let amount_projects = project_store.projects.len();
    let mut total_builds = 0;
    let mut total_pipelines = 0;
    for (_, project) in project_store.projects.iter() {
        total_builds += project.builds.len();
        total_pipelines += project.pipelines.len();
    }

    println!("Projects: {amount_projects}");
    println!("Pipelines: {total_pipelines}");
    println!("Builds: {total_builds}");
    let mut running_pipelines = 0;
    for (_, project) in project_store.projects.iter() {
        for (_, pipelines) in project.pipelines().iter() {
            if let Some(pipeline) = pipelines.iter().last() {
                if pipeline.is_active() {
                    running_pipelines += 1;
                }
            }
        }
    }
    println!("Active Pipelines: {running_pipelines}");
    println!();

    for (project_id, project) in project_store.projects.iter() {
        let pipelines = project.pipelines();
        for (_, pipelines) in pipelines.iter() {
            if let Some(pipeline) = pipelines.iter().last() {
                if pipeline.is_active() {
                    let summary = json!({
                        "id": project_id,
                        "url": project.url(),
                    });
                    println!("Project {}", serde_json::to_string(&summary)?);

                    let summary = json!({
                        "id": pipeline.id,
                        "state": pipeline.state,
                        "created_at": pipeline.created_at,
                        "finished_at": pipeline.finished_at,
                    });
                    println!("  Pipeline {}", serde_json::to_string(&summary)?);

                    // using a BTreeMap to filter only the last job by its name and for each name
                    //
                    // by using a BTreeMap instead of a HashMap this also sorts the jobs
                    let mut job_map: BTreeMap<&str, &Job> = BTreeMap::new();
                    for job in pipeline.jobs.iter() {
                        job_map.insert(job.name.as_str(), job);
                    }

                    // printing from the BTreeMap results in printing only the latest job
                    let all_job_states = JobState::all_job_states();
                    for state in all_job_states.iter() {
                        for (_, job) in job_map.iter() {
                            if *state == job.state {
                                let summary = json!({
                                    "id": job.id,
                                    "state": job.state,
                                    "ref": job.r#ref,
                                    "name": job.name,
                                    "runner": job.runner,
                                });
                                println!("    Build {}", serde_json::to_string(&summary)?);
                            }
                        }
                    }
                    println!();
                }
            }
        }
    }

    Ok(())
}

async fn load_data(project_store: &mut ProjectStore, cli_args: &CliArgs) -> Result<()> {
    let builds = query_builds(cli_args.into()).await?;
    for build in builds {
        project_store.push_event(Webhook::Build(Box::new(build)));
    }

    let pipelines = query_pipelines(cli_args.into()).await?;
    for pipeline in pipelines {
        project_store.push_event(Webhook::Pipeline(Box::new(pipeline)));
    }

    Ok(())
}
