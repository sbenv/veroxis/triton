use color_eyre::Result;

use crate::request::{
    query_builds, query_deployments, query_feature_flags, query_issues, query_merge_requests,
    query_notes, query_pipelines, query_pushs, query_releases, query_wiki_pages,
};
use crate::CliArgs;

pub async fn builds(cli_args: &CliArgs) -> Result<()> {
    let builds = query_builds(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&builds)?);
    Ok(())
}

pub async fn deployments(cli_args: &CliArgs) -> Result<()> {
    let deployments = query_deployments(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&deployments)?);
    Ok(())
}

pub async fn feature_flags(cli_args: &CliArgs) -> Result<()> {
    let feature_flags = query_feature_flags(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&feature_flags)?);
    Ok(())
}

pub async fn issues(cli_args: &CliArgs) -> Result<()> {
    let issues = query_issues(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&issues)?);
    Ok(())
}

pub async fn merge_request(cli_args: &CliArgs) -> Result<()> {
    let merge_requests = query_merge_requests(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&merge_requests)?);
    Ok(())
}

pub async fn notes(cli_args: &CliArgs) -> Result<()> {
    let notes = query_notes(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&notes)?);
    Ok(())
}

pub async fn pipelines(cli_args: &CliArgs) -> Result<()> {
    let pipelines = query_pipelines(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&pipelines)?);
    Ok(())
}

pub async fn pushs(cli_args: &CliArgs) -> Result<()> {
    let pushs = query_pushs(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&pushs)?);
    Ok(())
}

pub async fn releases(cli_args: &CliArgs) -> Result<()> {
    let releases = query_releases(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&releases)?);
    Ok(())
}

pub async fn wiki_pages(cli_args: &CliArgs) -> Result<()> {
    let wiki_pages = query_wiki_pages(cli_args.into()).await?;
    println!("{}", serde_json::to_string(&wiki_pages)?);
    Ok(())
}
