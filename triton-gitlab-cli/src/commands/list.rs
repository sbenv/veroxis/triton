use color_eyre::Result;
use serde_json::json;
use tracing::debug;

use crate::gitlab::GitlabClient;
use crate::GitlabCliArgs;

pub async fn list(args: GitlabCliArgs) -> Result<()> {
    let client = GitlabClient::new(args.gitlab_v4_url, args.gitlab_token);
    let projects = client.query_projects().await?;
    for project in projects.as_array().unwrap().iter() {
        let project_url = project["web_url"].as_str().unwrap();
        if let Some(filter) = &args.filter {
            if !project_url.contains(filter) {
                debug!("skipped {project_url} due to filter");
                continue;
            }
        }
        let hooks = client.list_hooks(project["id"].as_u64().unwrap()).await?;
        let has_triton_hook = hooks
            .as_array()
            .unwrap()
            .iter()
            .filter(|hook| hook["url"].as_str().unwrap() == args.triton_webhook)
            .count()
            > 0;
        let summary = json!({
            "id": project["id"].as_u64().unwrap(),
            "url": project["web_url"].as_str().unwrap(),
            "has_triton_hook": has_triton_hook,
        });
        println!("{summary}");
    }
    Ok(())
}
