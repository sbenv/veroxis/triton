use std::io::stdin;

use color_eyre::Result;
use tracing::debug;

use crate::gitlab::GitlabClient;
use crate::GitlabCliArgs;

pub async fn delete(args: GitlabCliArgs) -> Result<()> {
    let triton_webhook = args.triton_webhook.as_str();
    let gitlab_token = args.gitlab_token.as_str();
    let client = GitlabClient::new(args.gitlab_v4_url.clone(), gitlab_token.to_string());
    let projects = client.query_projects().await?;
    for project in projects.as_array().unwrap().iter() {
        let project_url = project["web_url"].as_str().unwrap();
        if let Some(filter) = &args.filter {
            if !project_url.contains(filter) {
                debug!("skipped {project_url} due to filter");
                continue;
            }
        }
        println!("{project_url}");

        let hooks = client.list_hooks(project["id"].as_u64().unwrap()).await?;
        for hook in hooks.as_array().unwrap().iter() {
            let id = hook["id"].as_u64().unwrap();
            let url = hook["url"].as_str().unwrap();
            if url == triton_webhook {
                let mut delete_hook = false;
                if args.yes {
                    delete_hook = true;
                } else {
                    println!("Found matching webhook. Do you want to delete it? y/n");
                    let mut success = false;
                    while !success {
                        let mut buffer = String::new();
                        stdin().read_line(&mut buffer)?;
                        match buffer.trim_end() {
                            "y" => {
                                delete_hook = true;
                                success = true;
                            }
                            "n" => {
                                delete_hook = false;
                                success = true;
                            }
                            _ => println!("Please answer with y or n"),
                        }
                    }
                }
                if delete_hook {
                    println!("deleting hook...");
                    match client
                        .delete_hook(
                            project["id"].as_u64().unwrap(),
                            gitlab_token.to_string(),
                            id,
                        )
                        .await?
                    {
                        true => println!("hook was deleted"),
                        false => println!("failed to delete hook"),
                    }
                }
            }
        }
    }

    Ok(())
}
