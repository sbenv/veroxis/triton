use std::io::stdin;

use color_eyre::Result;
use tracing::debug;

use crate::gitlab::GitlabClient;
use crate::GitlabCliArgs;

pub async fn create(args: GitlabCliArgs) -> Result<()> {
    let triton_webhook = args.triton_webhook.as_str();
    let gitlab_token = args.gitlab_token.as_str();
    let triton_token = args.triton_token.clone();
    let client = GitlabClient::new(args.gitlab_v4_url.clone(), gitlab_token.to_string());
    let projects = client.query_projects().await?;
    for project in projects.as_array().unwrap().iter() {
        let project_url = project["web_url"].as_str().unwrap();
        if let Some(filter) = &args.filter {
            if !project_url.contains(filter) {
                debug!("skipped {project_url} due to filter");
                continue;
            }
        }
        println!("{project_url}");

        let hooks = client.list_hooks(project["id"].as_u64().unwrap()).await?;
        let has_triton_hook = hooks
            .as_array()
            .unwrap()
            .iter()
            .filter(|hook| hook["url"].as_str().unwrap() == triton_webhook)
            .count()
            > 0;

        if !has_triton_hook {
            let mut create_hook = false;
            if args.yes {
                create_hook = true;
            } else {
                println!(
                    "Project doesn't have a Webhhok yet: {}",
                    project["web_url"].as_str().unwrap()
                );
                println!("Do you want to create it? y/n");
                let mut success = false;
                while !success {
                    let mut buffer = String::new();
                    stdin().read_line(&mut buffer)?;
                    match buffer.trim_end() {
                        "y" => {
                            create_hook = true;
                            success = true;
                        }
                        "n" => {
                            create_hook = false;
                            success = true;
                        }
                        _ => println!("Please answer with y or n"),
                    }
                }
            }
            if create_hook {
                println!("setting up hook...");
                match client
                    .add_hook(
                        project["id"].as_u64().unwrap(),
                        gitlab_token.to_string(),
                        triton_webhook.to_string(),
                        triton_token.clone(),
                    )
                    .await?
                {
                    true => println!("hook was created"),
                    false => println!("failed to create hook"),
                }
            }
        }
    }

    Ok(())
}
