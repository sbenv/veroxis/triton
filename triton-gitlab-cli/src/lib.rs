use clap::Parser;
use color_eyre::Result;

pub mod commands;
pub mod gitlab;

#[derive(Debug, Parser)]
#[clap(name = "commands", about)]
pub enum GitlabCliCommand {
    #[clap(name = "list")]
    List,

    #[clap(name = "create")]
    Create,

    #[clap(name = "delete")]
    Delete,
}

#[derive(Debug, Parser)]
#[clap(name = "triton-gitlab-cli", author, version, about, long_about = None)]
pub struct GitlabCliArgs {
    /// gitlab api token with permissions to the webhooks in projects
    #[clap(long)]
    pub gitlab_token: String,

    /// the gitlab v4 api url e.g. `https://gitlab.com/api/v4`
    #[clap(long)]
    pub gitlab_v4_url: String,

    /// the port on which the gitlab-server listens
    #[clap(long)]
    pub triton_webhook: String,

    /// tritons security token which will be passed from gitlab to triton when sending webhook events
    #[clap(long)]
    pub triton_token: Option<String>,

    /// apply a substring filter on detected projects
    #[clap(long)]
    pub filter: Option<String>,

    /// skip interactively asking and assume `yes` for every create/delete action
    #[clap(long)]
    pub yes: bool,

    #[clap(subcommand)]
    pub command: GitlabCliCommand,
}

pub async fn run(args: GitlabCliArgs) -> Result<()> {
    match args.command {
        GitlabCliCommand::List => commands::list::list(args).await?,
        GitlabCliCommand::Create => commands::create::create(args).await?,
        GitlabCliCommand::Delete => commands::delete::delete(args).await?,
    };

    Ok(())
}
