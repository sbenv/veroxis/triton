use color_eyre::Result;
use serde_json::json;

#[derive(Debug)]
pub struct GitlabClient {
    gitlab_apiv4_url: String,
    gitlab_token: String,
}

impl GitlabClient {
    pub fn new(gitlab_apiv4_url: String, gitlab_token: String) -> Self {
        Self {
            gitlab_apiv4_url,
            gitlab_token,
        }
    }

    pub async fn query_projects(&self) -> Result<serde_json::Value> {
        const MAINTAINER: &str = "40";
        const MAX_ALLOWED_BY_GITLAB: &str = "50000";
        let gitlab_apiv4_url = self.gitlab_apiv4_url.as_str();
        let url = format!("{gitlab_apiv4_url}/projects?archived=false&min_access_level={MAINTAINER}&order_by=id&per_page={MAX_ALLOWED_BY_GITLAB}");
        let response = Self::query_json(url.as_str(), &self.gitlab_token).await?;
        Ok(response)
    }

    pub async fn list_hooks(&self, project_id: u64) -> Result<serde_json::Value> {
        let gitlab_apiv4_url = self.gitlab_apiv4_url.as_str();
        let url = format!("{gitlab_apiv4_url}/projects/{project_id}/hooks");
        let response = Self::query_json(url.as_str(), &self.gitlab_token).await?;
        Ok(response)
    }

    async fn query_json(url: &str, token: &str) -> Result<serde_json::Value> {
        let response = reqwest::Client::builder()
            .build()?
            .get(url)
            .header("PRIVATE-TOKEN", token)
            .send()
            .await?
            .json::<serde_json::Value>()
            .await?;
        Ok(response)
    }

    pub async fn add_hook(
        &self,
        project_id: u64,
        gitlab_token: String,
        triton_url: String,
        triton_token: Option<String>,
    ) -> Result<bool> {
        let gitlab_apiv4_url = self.gitlab_apiv4_url.as_str();
        let url = format!("{gitlab_apiv4_url}/projects/{project_id}/hooks");
        let mut body = json!({
            "id": project_id,
            "url": triton_url,
            "enable_ssl_verification": true,
            "confidential_issues_events": true,
            "confidential_note_events": true,
            "deployment_events": true,
            "feature_flag_events": true,
            "issues_events": true,
            "job_events": true,
            "merge_requests_events": true,
            "note_events": true,
            "pipeline_events": true,
            "push_events_branch_filter": true,
            "push_events": true,
            "releases_events": true,
            "tag_push_events": true,
            "wiki_page_events": true,
        });
        if let Some(token) = triton_token {
            body["token"] = json!(token);
        }
        let response = reqwest::Client::builder()
            .build()?
            .post(url)
            .body(body.to_string())
            .header("PRIVATE-TOKEN", gitlab_token)
            .header("Content-Type", "application/json")
            .send()
            .await?;

        Ok(response.status().is_success())
    }

    pub async fn delete_hook(
        &self,
        project_id: u64,
        gitlab_token: String,
        hook_id: u64,
    ) -> Result<bool> {
        let gitlab_apiv4_url = self.gitlab_apiv4_url.as_str();
        let url = format!("{gitlab_apiv4_url}/projects/{project_id}/hooks/{hook_id}");
        let response = reqwest::Client::builder()
            .build()?
            .delete(url)
            .header("PRIVATE-TOKEN", gitlab_token)
            .send()
            .await?;

        Ok(response.status().is_success())
    }
}
