{
  description = "build env for rust projects";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };
  inputs.rust-overlay = {
    url = "github:oxalica/rust-overlay";
    inputs = {
      nixpkgs.follows = "nixpkgs";
      flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, flake-utils, nixpkgs, rust-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };

        rust = pkgs.rust-bin.stable.latest.default.override {
          extensions = [ "rust-src" ];
          targets = [ "x86_64-unknown-linux-musl" ];
        };
        cargoBuildInputs = with pkgs; lib.optionals stdenv.isDarwin [
          darwin.apple_sdk.frameworks.CoreServices
        ];
      in
      {
        packages = {};

        devShells.default = pkgs.mkShell {
          RUST_BACKTRACE = 1;
          RUST_LOG = "info";
          ZIG_GLOBAL_CACHE_DIR = "./target/zig_cache/global";
          ZIG_LOCAL_CACHE_DIR = "./target/zig_cache/local";

          buildInputs = ([
            rust
            cargoBuildInputs
            pkgs.cargo-edit
            pkgs.cargo-nextest
            pkgs.cargo-zigbuild
            pkgs.pkg-config
            pkgs.just
            pkgs.zig
          ]);

          shellHook = ''
          '';
        };
      }
    );
}
