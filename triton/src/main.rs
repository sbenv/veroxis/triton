use std::io::IsTerminal;

use clap::Parser;
use color_eyre::Result;
use tracing_subscriber::util::SubscriberInitExt;
use triton_cli::CliArgs;
use triton_gitlab_cli::GitlabCliArgs;
use triton_gitlab_server::GitlabServerArgs;

#[cfg(target_os = "windows")]
#[global_allocator]
static ALLOC: mimalloc::MiMalloc = mimalloc::MiMalloc;

#[cfg(target_os = "linux")]
#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

#[derive(Debug, Parser)]
#[clap(name = "triton", author, version, about, long_about = None)]
pub struct TritonArguments {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Parser, Debug)]
#[clap(name = "commands", about)]
pub enum Commands {
    /// run the gitlab-server
    #[clap(name = "gitlab-server")]
    GitlabServer(GitlabServerArgs),

    /// run the gitlab-cli
    #[clap(name = "gitlab-cli")]
    GitlabCli(GitlabCliArgs),

    /// run the cli
    #[clap(name = "cli")]
    Cli(CliArgs),
}

#[tokio::main]
async fn main() -> Result<()> {
    color_eyre::install()?;
    let triton_arguments = TritonArguments::parse();

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info");
    }
    let (non_blocking, _guard) = tracing_appender::non_blocking(std::io::stderr());

    match triton_arguments.command {
        Commands::GitlabServer(args) => {
            tracing_subscriber::fmt()
                .with_writer(non_blocking)
                .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
                .event_format(tracing_subscriber::fmt::format().json())
                .finish()
                .init();
            triton_gitlab_server::run(args).await?;
        }
        Commands::GitlabCli(args) => {
            tracing_subscriber::fmt()
                .with_writer(non_blocking)
                .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
                .with_ansi(std::io::stderr().is_terminal())
                .finish()
                .init();
            triton_gitlab_cli::run(args).await?;
        }
        Commands::Cli(args) => {
            tracing_subscriber::fmt()
                .with_writer(non_blocking)
                .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
                .with_ansi(std::io::stderr().is_terminal())
                .finish()
                .init();
            triton_cli::run(args).await?;
        }
    };

    Ok(())
}
