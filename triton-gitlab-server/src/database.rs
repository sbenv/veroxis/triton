use std::sync::Arc;
use std::time::Duration;

use axum::async_trait;
use chrono::prelude::*;
use color_eyre::eyre::Result;
use futures::TryStreamExt;
use mongodb::bson::doc;
use mongodb::options::{ClientOptions, FindOptions, IndexOptions};
use mongodb::{Client, IndexModel};
use serde_json::Value;
use tokio::sync::broadcast::Receiver;
use tokio::task::JoinHandle;
use tracing::log::debug;

use crate::types::timed_mongo_entry::TimedMongoEntry;

#[async_trait]
pub trait TritonDB: Send + Sync + std::fmt::Debug {
    async fn fetch_webhook_errors(
        &self,
        min_date: Option<DateTime<Utc>>,
        max_date: Option<DateTime<Utc>>,
    ) -> Result<Vec<Value>>;

    async fn fetch_webhook_events(
        &self,
        object_kind: Option<&str>,
        min_date: Option<DateTime<Utc>>,
        max_date: Option<DateTime<Utc>>,
    ) -> Result<Vec<Value>>;

    async fn ping(&self) -> Result<()>;
    async fn add_webhook(&self, webhook: Value) -> Result<()>;
}

pub type DynTritonDB = Arc<dyn TritonDB>;

#[derive(Debug, Clone)]
pub struct MongoDB {
    client: Client,
    database_name: String,
    ttl_error_log: Option<u64>,
    ttl_webhook_events: Option<u64>,
}

pub const COLLECTION_WEBHOOK_EVENTS: &str = "webhook_events";
pub const COLLECTION_GITLAB_ERROR_LOG: &str = "errors";

impl MongoDB {
    #[allow(clippy::too_many_arguments)]
    pub async fn new(
        user: &str,
        pass: &str,
        host: &str,
        port: u16,
        database_name: &str,
        ttl_error_log: Option<u64>,
        ttl_webhook_events: Option<u64>,
    ) -> Result<Self, mongodb::error::Error> {
        let connection_string = format!("mongodb://{user}:{pass}@{host}:{port}");
        let mut client_options = ClientOptions::parse(connection_string.as_str()).await?;
        client_options.app_name = Some("Triton".to_owned());
        let mongo_client = Client::with_options(client_options)?;
        let database = Self {
            client: mongo_client,
            database_name: database_name.to_owned(),
            ttl_error_log,
            ttl_webhook_events,
        };
        Ok(database)
    }

    #[rustfmt::skip]
    pub async fn init_db(&self) -> Result<(), mongodb::error::Error> {
        self.init_collection(COLLECTION_WEBHOOK_EVENTS, self.ttl_webhook_events).await?;
        self.init_collection(COLLECTION_GITLAB_ERROR_LOG, self.ttl_error_log).await?;
        Ok(())
    }

    async fn init_collection(
        &self,
        name: &str,
        ttl: Option<u64>,
    ) -> Result<(), mongodb::error::Error> {
        let mongo_client = self.client.clone();
        let db = mongo_client.database(&self.database_name);

        // create db if it doesn't exist yet by creating the collection `name` within it
        let collections = db.list_collection_names(None).await?;
        if !collections.contains(&name.to_owned()) {
            db.create_collection(name, None).await?;
        }

        // set ttl for each entry with field `mongo_entry_created_at`
        if let Some(ttl) = ttl {
            self.set_ttl(name, &db, ttl).await?;
        }

        Ok(())
    }

    async fn set_ttl(
        &self,
        name: &str,
        db: &mongodb::Database,
        ttl: u64,
    ) -> Result<(), mongodb::error::Error> {
        static TTL_INDEX_NAME: &str = "mongo_entry_created_at";
        let ttl_index_name_suffixed = format!("{TTL_INDEX_NAME}_1");
        let collection = db.collection::<TimedMongoEntry<Value>>(name);

        let indices = collection.list_index_names().await?;

        for index in indices.iter() {
            if index == &ttl_index_name_suffixed {
                collection
                    .drop_index(ttl_index_name_suffixed.as_str(), None)
                    .await?;
                tracing::debug!("dropped TTL index: {name} {ttl}");
            }
        }

        let mut index_model = IndexModel::default();
        index_model.keys = doc! {TTL_INDEX_NAME: 1};

        let mut index_options = IndexOptions::default();
        index_options.expire_after = Some(Duration::from_secs(ttl));
        index_model.options = Some(index_options);

        collection.create_index(index_model, None).await?;
        tracing::debug!("created TTL index: {name} {ttl}");
        Ok(())
    }

    pub async fn add_error(&self, webhook_parse_error: Value) -> Result<(), mongodb::error::Error> {
        self.add_entry(webhook_parse_error, COLLECTION_GITLAB_ERROR_LOG)
            .await?;
        Ok(())
    }

    async fn add_entry(&self, entry: Value, collection: &str) -> Result<(), mongodb::error::Error> {
        let mongo_entry = TimedMongoEntry::<Value>::new(entry);
        let mongo_client = self.client.clone();
        let db = mongo_client.database(&self.database_name);
        let collection = db.collection::<TimedMongoEntry<Value>>(collection);
        collection.insert_one(mongo_entry, None).await?;
        Ok(())
    }
}

#[async_trait]
impl TritonDB for MongoDB {
    async fn add_webhook(&self, webhook: Value) -> Result<()> {
        if webhook["object_kind"].as_str().is_some() {
            self.add_entry(webhook, COLLECTION_WEBHOOK_EVENTS).await?;
        } else {
            self.add_entry(webhook, COLLECTION_GITLAB_ERROR_LOG).await?;
        }

        Ok(())
    }

    async fn ping(&self) -> Result<()> {
        let mongo_client = self.client.clone();
        let _ = mongo_client.list_databases(None, None).await?;
        Ok(())
    }

    async fn fetch_webhook_errors(
        &self,
        min_date: Option<DateTime<Utc>>,
        max_date: Option<DateTime<Utc>>,
    ) -> Result<Vec<Value>> {
        let mongo_client = self.client.clone();
        let db = mongo_client.database(&self.database_name);
        let typed_collection = db.collection::<Value>(COLLECTION_GITLAB_ERROR_LOG);
        let mut error_collection = vec![];

        let filter = match (min_date, max_date) {
            (None, None) => None,
            (None, Some(max_date)) => Some(doc! {
                "mongo_entry_created_at": {
                    "$lte": max_date,
                }
            }),
            (Some(min_date), None) => Some(doc! {
                "mongo_entry_created_at": {
                    "$gte": min_date,
                }
            }),
            (Some(min_date), Some(max_date)) => Some(doc! {
                "mongo_entry_created_at": {
                    "$gte": min_date,
                    "$lte": max_date,
                }
            }),
        };

        let mut cursor = typed_collection.find(filter, None).await?;

        while let Some(error) = cursor.try_next().await? {
            error_collection.push(error);
        }

        Ok(error_collection)
    }

    async fn fetch_webhook_events(
        &self,
        object_kind: Option<&str>,
        min_date: Option<DateTime<Utc>>,
        max_date: Option<DateTime<Utc>>,
    ) -> Result<Vec<Value>> {
        let mongo_client = self.client.clone();
        let db = mongo_client.database(&self.database_name);

        let typed_collection = db.collection::<Value>(COLLECTION_WEBHOOK_EVENTS);

        let mut webhook_event_collection = vec![];

        let mut filter = match (min_date, max_date) {
            (None, None) => None,
            (None, Some(max_date)) => Some(doc! {
                "mongo_entry_created_at": {
                    "$lte": max_date,
                }
            }),
            (Some(min_date), None) => Some(doc! {
                "mongo_entry_created_at": {
                    "$gte": min_date,
                }
            }),
            (Some(min_date), Some(max_date)) => Some(doc! {
                "mongo_entry_created_at": {
                    "$gte": min_date,
                    "$lte": max_date,
                }
            }),
        };
        if let Some(object_kind) = object_kind {
            filter = match filter {
                Some(mut filter) => {
                    filter.insert("object_kind", object_kind);
                    Some(filter)
                }
                None => Some(doc! {
                    "object_kind": object_kind
                }),
            };
        }

        let find_options = FindOptions::builder()
            .projection(doc! {"_id": 0, "mongo_entry_created_at": 0})
            .build();

        let mut cursor = typed_collection.find(filter, Some(find_options)).await?;

        while let Some(webhook_event) = cursor.try_next().await? {
            webhook_event_collection.push(webhook_event);
        }

        Ok(webhook_event_collection)
    }
}

pub fn run_async_database_inserter(
    mut json_rx: Receiver<Value>,
    database: Arc<dyn TritonDB>,
) -> JoinHandle<()> {
    tokio::spawn(async move {
        loop {
            match json_rx.recv().await {
                Ok(event_data) => {
                    debug!("[database] inserting webhook data");
                    let add_webhook_result = database.add_webhook(event_data).await;
                    if let Err(mongo_error) = add_webhook_result {
                        tracing::error!("{:?}", mongo_error);
                    }
                }
                Err(error) => tracing::error!("failed to receive event_data: {}", error),
            }
        }
    })
}
