use std::process::Command;

use mlua::prelude::*;

pub fn register(lua: &Lua) -> Result<(), mlua::Error> {
    let globals = lua.globals();

    let rust_shell_exec =
        lua.create_function(|_, (cmd, args): (String, Vec<String>)| -> LuaResult<()> {
            if let Err(err) = Command::new(cmd).args(args).status() {
                tracing::error!("[LUA] shell_exec: {:?}", err);
            }
            Ok(())
        })?;
    globals.set("shell_exec", rust_shell_exec)?;

    Ok(())
}
