use mlua::prelude::*;

pub fn register(lua: &Lua) -> Result<(), mlua::Error> {
    let globals = lua.globals();

    let rust_http_request = lua.create_function(
        |_, _url: String| -> LuaResult<(Option<String>, Option<String>, u16)> {
            tracing::info!("http_request is not yet implemented");
            Ok((None, None, 0))
        },
    )?;
    globals.set("http_request", rust_http_request)?;

    Ok(())
}
