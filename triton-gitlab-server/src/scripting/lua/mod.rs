use std::path::PathBuf;
use std::thread;

use mlua::prelude::*;
use serde_json::Value;
use tokio::sync::broadcast::Receiver;
use tokio::task::JoinHandle;

pub mod http;
pub mod log;
pub mod shell;

pub fn run_async_lua_executor(
    mut json_rx: Receiver<Value>,
    scripts: Vec<PathBuf>,
) -> JoinHandle<()> {
    tokio::spawn(async move {
        loop {
            match json_rx.recv().await {
                Ok(event_data) => {
                    let cloned_scripts = scripts.clone();
                    tokio::spawn(async move {
                        run(cloned_scripts, event_data).await;
                    });
                }
                Err(error) => tracing::error!("failed to receive event_data: {}", error),
            }
        }
    })
}

pub async fn run(scripts: Vec<PathBuf>, json: serde_json::Value) {
    let mut handlers = vec![];
    for script_path in scripts.iter() {
        tracing::debug!("[LUA] executing {:?}", &script_path);
        match std::fs::read_to_string(script_path) {
            Ok(lua_code) => {
                let json_clone = json.clone();
                let script_path_clone = script_path.clone();
                // lua is executed in its own thread for sandboxing
                // and to provide its own isolated ENV
                //
                // the thread is also used to parallelize executing scripts
                let handler = thread::spawn(move || {
                    let run_lua_thread = || -> Result<(), Box<dyn std::error::Error>> {
                        if let Some(required_workdir) = script_path_clone.parent() {
                            std::env::set_current_dir(required_workdir)?;
                        }
                        let lua = Lua::new();
                        setup_lua_env(&lua, &json_clone)?;
                        lua.load(lua_code.as_str()).exec()?;
                        Ok(())
                    };
                    if let Err(err) = run_lua_thread() {
                        tracing::error!("[LUA] {script_path_clone:?}: {:?}", err);
                    }
                });
                handlers.push(handler);
            }
            Err(err) => tracing::error!("{script_path:?}: {:?}", err),
        };
    }
    for handler in handlers.into_iter() {
        if let Err(error) = handler.join() {
            tracing::error!("[LUA] thread join failed: {:?}", error);
        }
    }
}

fn setup_lua_env(lua: &Lua, json: &serde_json::Value) -> Result<(), mlua::Error> {
    let globals = lua.globals();

    globals.set("request_json", lua.to_value(&json.to_string())?)?;
    globals.set("request_data", lua.to_value(&json)?)?;

    log::register(lua)?;
    http::register(lua)?;
    shell::register(lua)?;

    Ok(())
}
