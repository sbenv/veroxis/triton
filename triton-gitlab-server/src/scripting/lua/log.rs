use mlua::prelude::*;

/// register logging methods
///
/// ```lua
/// log_info("foo")
/// log_debug("foo")
/// log_warn("foo")
/// log_error"foo")
/// ```
pub fn register(lua: &Lua) -> Result<(), mlua::Error> {
    let globals = lua.globals();

    let rust_log_info = lua.create_function(|_, msg: Option<String>| -> LuaResult<()> {
        let msg = match msg {
            Some(msg) => msg,
            None => "nil".to_owned(),
        };
        tracing::info!("{}", msg);
        Ok(())
    })?;
    globals.set("log_info", rust_log_info)?;

    let rust_log_debug = lua.create_function(|_, msg: Option<String>| -> LuaResult<()> {
        let msg = match msg {
            Some(msg) => msg,
            None => "nil".to_owned(),
        };
        tracing::debug!("{}", msg);
        Ok(())
    })?;
    globals.set("log_debug", rust_log_debug)?;

    let rust_log_warn = lua.create_function(|_, msg: Option<String>| -> LuaResult<()> {
        let msg = match msg {
            Some(msg) => msg,
            None => "nil".to_owned(),
        };
        tracing::warn!("{}", msg);
        Ok(())
    })?;
    globals.set("log_warn", rust_log_warn)?;

    let rust_log_error = lua.create_function(|_, msg: Option<String>| -> LuaResult<()> {
        let msg = match msg {
            Some(msg) => msg,
            None => "nil".to_owned(),
        };
        tracing::error!("{}", msg);
        Ok(())
    })?;
    globals.set("log_error", rust_log_error)?;

    Ok(())
}
