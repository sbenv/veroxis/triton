use chrono::{DateTime, Utc};
use mongodb::bson::serde_helpers::chrono_datetime_as_bson_datetime;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct TimedMongoEntry<T> {
    #[serde(with = "chrono_datetime_as_bson_datetime")]
    mongo_entry_created_at: DateTime<Utc>,

    #[serde(flatten)]
    data: T,
}

impl<T> TimedMongoEntry<T> {
    pub fn new(data: T) -> Self {
        Self {
            mongo_entry_created_at: Utc::now(),
            data,
        }
    }
}
