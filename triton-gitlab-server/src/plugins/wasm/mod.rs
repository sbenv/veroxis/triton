use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;

use color_eyre::eyre::Result;
use tokio::sync::broadcast::Receiver;
use tokio::sync::RwLock;
use tokio::task::JoinHandle;
use tokio::time::sleep;

pub fn run_async_wasm_executor(
    json_rx: Receiver<serde_json::Value>,
    plugin_paths: Vec<PathBuf>,
) -> JoinHandle<()> {
    tokio::spawn(async move {
        let wasm_index = Arc::new(WasmIndex::new());
        let mut tasks = vec![];
        let webhook_listener = tokio::spawn(run_json_listener(wasm_index.clone(), json_rx));
        tasks.push(webhook_listener);
        let module_indexer = tokio::spawn(run_module_indexer(wasm_index, plugin_paths));
        tasks.push(module_indexer);
        let results = futures::future::join_all(tasks).await;
        for result in results {
            if let Err(error) = result {
                tracing::error!("JoinError: {error}");
            }
        }
    })
}

async fn run_module_indexer(wasm_index: Arc<WasmIndex>, plugin_paths: Vec<PathBuf>) {
    loop {
        let mut module_index = wasm_index.modules.read().await.clone();
        let mut detected_files = vec![];
        for plugin_dir_path in plugin_paths.iter() {
            let mut read_dir = match tokio::fs::read_dir(&plugin_dir_path).await {
                Ok(read_dir) => read_dir,
                Err(error) => {
                    tracing::error!("failed to read directory `{plugin_dir_path:?}`: {error}");
                    continue;
                }
            };
            let mut dir_entries = vec![];
            while let Ok(Some(entry)) = read_dir.next_entry().await {
                dir_entries.push(entry);
            }

            // sort for OsString is... complicated
            //
            // this is a simple best-effort
            dir_entries.sort_by(|a, b| {
                if let (Some(a), Some(b)) = (a.file_name().to_str(), b.file_name().to_str()) {
                    if let Some(ord) = a.partial_cmp(b) {
                        return ord;
                    }
                }
                std::cmp::Ordering::Equal
            });

            for entry in dir_entries {
                let file_path = entry.path();
                let file_path_lossy = file_path.to_string_lossy();
                if !file_path_lossy.ends_with(".wasm") {
                    tracing::info!("skipping non-wasm file: {file_path:?}");
                    continue;
                }
                let metadata = match entry.metadata().await {
                    Ok(metadata) => metadata,
                    Err(error) => {
                        tracing::error!(
                            "failed to read metadata of `{plugin_dir_path:?}`: {error}"
                        );
                        continue;
                    }
                };
                if !metadata.is_file() {
                    tracing::info!("not a file: {file_path:?}");
                    continue;
                }
                detected_files.push(file_path.clone());
                let wasm_bytes = match tokio::fs::read(&file_path).await {
                    Ok(wasm_bytes) => wasm_bytes,
                    Err(error) => {
                        tracing::error!("failed to read wasm as bytes `{file_path:?}`: {error}");
                        continue;
                    }
                };
                match module_index.get_mut(&file_path) {
                    Some(wasm_module) => {
                        if wasm_bytes != wasm_module.bytes {
                            tracing::info!("updating module: {file_path:?}");
                            let module =
                                match wasmtime::Module::new(&wasm_index.engine, &wasm_bytes) {
                                    Ok(module) => module,
                                    Err(error) => {
                                        tracing::error!(
                                            "failed to compile module {file_path:?}: {error}"
                                        );
                                        continue;
                                    }
                                };
                            *wasm_module = WasmModule {
                                bytes: wasm_bytes,
                                module,
                            };
                        }
                    }
                    None => {
                        tracing::info!("loading module: {file_path:?}");
                        let module = match wasmtime::Module::new(&wasm_index.engine, &wasm_bytes) {
                            Ok(module) => module,
                            Err(error) => {
                                tracing::error!("failed to compile module {file_path:?}: {error}");
                                continue;
                            }
                        };
                        module_index.insert(
                            file_path,
                            WasmModule {
                                bytes: wasm_bytes,
                                module,
                            },
                        );
                    }
                }
            }
        }
        // delete modules which aren't found in the fs anymore
        module_index.retain(|path, _| {
            let keep = detected_files.contains(path);
            if !keep {
                tracing::info!("unloading module: {path:?}");
            }
            keep
        });
        let mut modules = wasm_index.modules.write().await;
        *modules = module_index;
        drop(modules);
        sleep(Duration::from_secs(2)).await;
    }
}

async fn run_json_listener(wasm_index: Arc<WasmIndex>, mut json_rx: Receiver<serde_json::Value>) {
    loop {
        match json_rx.recv().await {
            Ok(event_data) => {
                if let Err(error) = wasm_index.call_wasm_modules(event_data).await {
                    tracing::error!("wasm_index.call_wasm_modules: {error}");
                }
            }
            Err(error) => tracing::error!("failed to receive event_data: {}", error),
        }
    }
}

struct WasmIndex {
    engine: wasmtime::Engine,
    modules: tokio::sync::RwLock<HashMap<WasmModuleIndex, WasmModule>>,
}

type WasmModuleIndex = PathBuf;

#[derive(Clone)]
struct WasmModule {
    bytes: Vec<u8>,
    module: wasmtime::Module,
}

impl WasmIndex {
    pub fn new() -> WasmIndex {
        let mut config = wasmtime::Config::default();
        config.async_support(true);
        config.disable_cache();
        let engine = wasmtime::Engine::new(&config).unwrap();
        WasmIndex {
            engine,
            modules: RwLock::new(HashMap::new()),
        }
    }

    pub async fn call_wasm_modules(&self, json: serde_json::Value) -> Result<()> {
        let json_string = serde_json::to_string(&json)?;
        let modules = self.modules.read().await;
        for (index, wasm_module) in modules.iter() {
            tracing::debug!("{index:?}: start");
            let module = &wasm_module.module;
            let mut linker: wasmtime::Linker<wasi_common::WasiCtx> =
                wasmtime::Linker::new(&self.engine);
            if let Err(error) = wasi_common::tokio::add_to_linker(&mut linker, |s| s) {
                tracing::error!("{index:?}: wasi_common::tokio::add_to_linker: {error}");
                continue;
            }
            let mut wasi_ctx_builder = wasi_common::tokio::WasiCtxBuilder::new();
            wasi_ctx_builder.inherit_stdio().inherit_stderr();
            if let Err(error) = wasi_ctx_builder.arg(json_string.as_str()) {
                tracing::error!("{index:?}: wasi_ctx_builder.arg: {error}");
                continue;
            }
            let wasi_ctx = wasi_ctx_builder.build();
            let mut store = wasmtime::Store::new(&self.engine, wasi_ctx);
            if let Err(error) = linker.module_async(&mut store, "", module).await {
                tracing::error!("{index:?}: linker.module_async: {error}");
                continue;
            };
            let function = match linker.get_default(&mut store, "") {
                Ok(function) => function,
                Err(error) => {
                    tracing::error!("{index:?}: linker.get_default: {error}");
                    continue;
                }
            };
            let function = match function.typed::<(), ()>(&store) {
                Ok(function) => function,
                Err(error) => {
                    tracing::error!("{index:?}: function.typed: {error}");
                    continue;
                }
            };
            tracing::debug!("{index:?}: execute");
            if let Err(error) = function.call_async(&mut store, ()).await {
                tracing::error!("{index:?}: function.call_async: {error}");
                continue;
            }
            tracing::debug!("{index:?}: done");
        }
        Ok(())
    }
}
