use axum::extract::{Query, State};
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Router;
use axum_macros::debug_handler;
use serde::Deserialize;
use serde_json::json;

use crate::database::DynTritonDB;

pub fn health_routes() -> Router<ReadyzState> {
    Router::new()
        .route("/livez", get(health_livez))
        .route("/readyz", get(health_readyz))
        .route("/healthz", get(health_readyz))
}

#[debug_handler]
async fn health_livez() -> impl IntoResponse {
    StatusCode::OK
}

#[derive(Deserialize)]
pub struct QueryArgs {
    verbose: Option<String>,
}

#[derive(Debug, Clone)]
pub struct ReadyzState {
    database: DynTritonDB,
}

impl ReadyzState {
    pub fn new(database: DynTritonDB) -> ReadyzState {
        ReadyzState { database }
    }
}

#[debug_handler]
pub async fn health_readyz(
    query_args: Query<QueryArgs>,
    State(readyz_config): State<ReadyzState>,
) -> impl IntoResponse {
    let mut status_code = StatusCode::OK;
    let database_status = match readyz_config.database.ping().await {
        Ok(_) => "ok",
        Err(_) => {
            status_code = StatusCode::SERVICE_UNAVAILABLE;
            "err"
        }
    };
    match query_args.verbose {
        Some(_) => {
            let json = json!({
                "webserver": "ok",
                "database": database_status
            });
            (status_code, serde_json::to_string_pretty(&json).unwrap())
        }
        None => (status_code, String::new()),
    }
}
