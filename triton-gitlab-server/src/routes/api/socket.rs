use axum::extract::ws::{Message, WebSocket};
use axum::extract::{Query, State, WebSocketUpgrade};
use axum::response::IntoResponse;
use axum_macros::debug_handler;
use serde::Deserialize;
use serde_json::Value;
use tokio::sync::broadcast::Receiver;

use super::{has_api_access, ApiState};

#[derive(Deserialize)]
pub struct QueryArgs {
    secret: Option<String>,
}

#[debug_handler]
pub async fn websocket_handler(
    ws: WebSocketUpgrade,
    query_args: Query<QueryArgs>,
    State(api_config): State<ApiState>,
) -> impl IntoResponse {
    ws.on_upgrade(move |socket| {
        webhook_socket(
            socket,
            api_config.json_tx.subscribe(),
            api_config,
            query_args,
        )
    })
}

async fn webhook_socket(
    mut socket: WebSocket,
    mut json_rx: Receiver<Value>,
    api_config: ApiState,
    query_args: Query<QueryArgs>,
) {
    // close the socket immediately if the connection was unauthorized
    if !has_api_access(query_args.secret.as_deref(), api_config.secret.as_deref()) {
        if let Err(error) = socket.close().await {
            tracing::error!("failed to close unauthorized socket: {}", error);
        }
        return;
    };

    let uuid = uuid::Uuid::new_v4();

    tracing::info!("[{uuid:?}] client connected");
    scopeguard::defer!({
        tracing::info!("[{uuid:?}] client disconnected");
    });

    // the socket only notifies about event data coming from the gitlab-webhook route
    //
    // it exclusively sends this data to the client
    loop {
        tokio::select! {
            Some(client_msg) = socket.recv() => {
                if let Ok(msg) = client_msg {
                    match msg {
                        Message::Text(data) => {
                            let data = data.trim();
                            tracing::info!("[{uuid:?}] received TEXT from client: {data}");
                        }
                        Message::Binary(data) => {
                            tracing::info!("[{uuid:?}] received BINARY from client: {data:?}");
                        }
                        Message::Ping(_) => {
                            if let Err(error) = socket.send(Message::Pong(vec![])).await {
                                tracing::error!("[{uuid:?}] failed to answer PING with PONG: {error}");
                            }
                        }
                        Message::Pong(_) => {}
                        Message::Close(_) => {
                            return;
                        }
                    }
                } else {
                    return;
                }
            },
            json = json_rx.recv() => {
                match json {
                    Ok(event_data) => {
                        if let Ok(json) = serde_json::to_string(&event_data) {
                            if socket.send(Message::Text(json)).await.is_err() {
                                return;
                            }
                        }
                    }
                    Err(error) => tracing::error!("failed to receive event_data: {}", error),
                }
            },
        };
    }
}
