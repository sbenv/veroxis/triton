use axum::extract::{Query, State};
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::Json;
use axum_macros::debug_handler;
use chrono::prelude::*;
use serde::Deserialize;
use tracing::error;

use super::ApiState;
use crate::routes::api::has_api_access;

#[derive(Deserialize)]
pub struct QueryArgs {
    /// minimum date in rfc 3339 format
    min_date: Option<DateTime<Utc>>,

    /// maximum date in rfc 3339 format
    max_date: Option<DateTime<Utc>>,

    secret: Option<String>,
}

#[debug_handler]
pub async fn webhook_events_all(
    query_args: Query<QueryArgs>,
    State(api_config): State<ApiState>,
) -> impl IntoResponse {
    if !has_api_access(query_args.secret.as_deref(), api_config.secret.as_deref()) {
        return (StatusCode::FORBIDDEN, axum::Json(vec![]));
    }
    match api_config
        .database
        .fetch_webhook_events(None, query_args.min_date, query_args.max_date)
        .await
    {
        Ok(pipelines) => (StatusCode::OK, Json(pipelines)),
        Err(error) => {
            error!("{error}");
            (StatusCode::SERVICE_UNAVAILABLE, Json(vec![]))
        }
    }
}
