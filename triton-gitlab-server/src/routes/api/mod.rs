use axum::routing::get;
use axum::Router;
use serde_json::Value;
use tokio::sync::broadcast::Sender;

use crate::database::DynTritonDB;
use crate::routes::api::socket::websocket_handler;

pub mod errors;
pub mod socket;
pub mod webhook_events_all;
pub mod webhook_events_by_kind;

pub fn api_routes() -> Router<ApiState> {
    Router::new()
        .route(
            "/webhook_events_by_kind",
            get(webhook_events_by_kind::webhook_events_by_kind),
        )
        .route(
            "/webhook_events_all",
            get(webhook_events_all::webhook_events_all),
        )
        .route("/errors", get(errors::errors))
        .route("/socket", get(websocket_handler))
}

#[derive(Debug, Clone)]
pub struct ApiState {
    secret: Option<String>,
    pub database: DynTritonDB,
    pub json_tx: Sender<Value>,
}

impl ApiState {
    pub fn new(secret: Option<String>, database: DynTritonDB, json_tx: Sender<Value>) -> Self {
        Self {
            secret,
            database,
            json_tx,
        }
    }
}

pub fn has_api_access(secret_provided: Option<&str>, secret_required: Option<&str>) -> bool {
    // if the second argument is Some()
    // then a secret is required
    if let Some(secret_required) = secret_required {
        match secret_provided {
            // if the first argument doesnt match, return forbidden
            Some(secret_provided) => {
                if secret_provided != secret_required {
                    return false;
                }
            }
            // if the first argument doesnt exist, return forbidden
            None => return false,
        }
    }
    true
}
