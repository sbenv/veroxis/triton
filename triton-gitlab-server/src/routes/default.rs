use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Router;
use axum_macros::debug_handler;

pub const ROUTE_DEFAULT: &str = "/";

pub fn default_routes() -> Router {
    Router::new()
        .route("/", get(default_route))
        .fallback(not_found)
}

#[debug_handler]
async fn default_route() -> impl IntoResponse {
    StatusCode::OK
}

#[debug_handler]
async fn not_found() -> impl IntoResponse {
    (StatusCode::NOT_FOUND, String::from("404 - Not Found"))
}

#[cfg(test)]
mod tests {
    use axum::body::Body;
    use axum::http::Request;
    use rstest::rstest;
    use tower::ServiceExt;

    use super::*;

    fn create_request(route: &str) -> Request<axum::body::Body> {
        Request::builder()
            .uri(route)
            .method(axum::http::Method::GET)
            .body(Body::empty())
            .unwrap()
    }

    #[rstest]
    #[tokio::test]
    async fn default_route_works() {
        let app = default_routes();
        let request = create_request(ROUTE_DEFAULT);
        let response = app.oneshot(request).await.unwrap();
        assert_eq!(response.status(), StatusCode::OK);
    }

    #[rstest]
    #[case("/foo")]
    #[case("/foo/bar")]
    #[case("/foo/bar/baz")]
    #[tokio::test]
    async fn not_found_works(#[case] route: &str) {
        let app = default_routes();
        let request = create_request(route);
        let response = app.oneshot(request).await.unwrap();
        assert_eq!(response.status(), StatusCode::NOT_FOUND);
    }
}
