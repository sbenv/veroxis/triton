use std::time::SystemTime;

use axum::extract::State;
use axum::http::{HeaderMap, StatusCode};
use axum::response::IntoResponse;
use axum::routing::post;
use axum::{Json, Router};
use axum_macros::debug_handler;
use serde_json::{json, Value};
use tokio::sync::broadcast::Sender;

pub const ROUTE_GITLAB_WEBHOOK: &str = "/gitlab-webhook";
pub const HEADER_GITLAB_TOKEN: &str = "x-gitlab-token";

pub fn webhook_routes() -> Router<GitlabWebhookState> {
    Router::new().route(ROUTE_GITLAB_WEBHOOK, post(gitlab_webhook))
}

#[derive(Debug, Clone)]
pub struct GitlabWebhookState {
    secret: Option<String>,
    json_tx: Sender<Value>,
}

impl GitlabWebhookState {
    pub fn new(secret: Option<String>, json_tx: Sender<Value>) -> Self {
        Self { secret, json_tx }
    }
}

/// the webhook request may never fail so it will always return `StatusCode::OK`
#[debug_handler]
pub async fn gitlab_webhook(
    headers: HeaderMap,
    State(webhook_config): State<GitlabWebhookState>,
    Json(mut payload): Json<Value>,
) -> impl IntoResponse {
    if webhook_config.secret.is_some() {
        let header_secret = headers
            .get(HEADER_GITLAB_TOKEN)
            .and_then(|h| h.to_str().ok());
        match (webhook_config.secret, header_secret) {
            (Some(server_secret), Some(header_secret))
                if server_secret.as_str() == header_secret => {}
            _ => {
                tracing::warn!(
                    "denied access for request: {:?} {}",
                    &headers,
                    payload.to_string()
                );
                return StatusCode::OK;
            }
        }
    }

    let timestamp = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap_or_default()
        .as_nanos();
    payload["triton_recv"] = json!(timestamp.to_string());
    if let Err(error) = webhook_config.json_tx.send(payload) {
        tracing::error!("failed to broadcast webhook: {error}");
    }

    StatusCode::OK
}

#[cfg(test)]
mod tests {
    use axum::http::{self, Request};
    use rstest::rstest;
    use tokio::sync::broadcast;
    use tokio::sync::broadcast::Receiver;
    use tower::ServiceExt;

    use super::*;

    fn create_app(secret: Option<&str>) -> (Receiver<Value>, Router) {
        let (json_tx, json_rx) = broadcast::channel::<Value>(255);
        let state = GitlabWebhookState {
            secret: secret.map(|s| s.to_owned()),
            json_tx,
        };
        let app: Router = webhook_routes().with_state(state);
        (json_rx, app)
    }

    fn create_webhook_request(
        request_secret: Option<&str>,
        body: String,
    ) -> Request<axum::body::Body> {
        let mut request = Request::builder()
            .uri(ROUTE_GITLAB_WEBHOOK)
            .method(http::Method::POST)
            .header(http::header::CONTENT_TYPE, "application/json");
        if let Some(request_secret) = request_secret {
            request = request.header(HEADER_GITLAB_TOKEN, request_secret);
        }
        request.body(axum::body::Body::from(body)).unwrap()
    }

    #[rstest]
    #[case(None, None)]
    #[case(None, Some(""))]
    #[case(Some(""), Some(""))]
    #[case(None, Some("foo"))]
    #[case(Some("foo"), Some("foo"))]
    #[case(Some("foo"), None)]
    #[case(Some(""), None)]
    #[tokio::test]
    async fn always_http_ok(
        #[case] request_secret: Option<&'static str>,
        #[case] server_secret: Option<&'static str>,
    ) {
        let (_json_rx, app) = create_app(server_secret);
        let request = create_webhook_request(request_secret, json!({}).to_string());
        let response = app.oneshot(request).await.unwrap();
        assert_eq!(response.status(), StatusCode::OK);
    }

    #[rstest]
    #[case(None, None, 1)]
    #[case(None, Some(""), 0)]
    #[case(Some(""), Some(""), 1)]
    #[case(None, Some("foo"), 0)]
    #[case(Some("foo"), Some("foo"), 1)]
    #[case(Some("foo"), None, 1)]
    #[case(Some(""), None, 1)]
    #[tokio::test]
    async fn expect_broadcast_messages_with_secrets(
        #[case] request_secret: Option<&'static str>,
        #[case] server_secret: Option<&'static str>,
        #[case] expect_broadcast_len: usize,
    ) {
        let (json_rx, app) = create_app(server_secret);
        let request = create_webhook_request(request_secret, json!({}).to_string());
        let _response = app.oneshot(request).await.unwrap();
        assert_eq!(json_rx.len(), expect_broadcast_len);
    }

    #[rstest]
    #[tokio::test]
    async fn webhook_receive_injects_timestamp_in_broadcast() {
        let (mut json_rx, app) = create_app(None);
        let request = create_webhook_request(None, json!({}).to_string());
        let _response = app.oneshot(request).await.unwrap();
        let json = json_rx.recv().await.unwrap();
        let triton_recv = json["triton_recv"].as_str().unwrap();
        assert!(!triton_recv.is_empty());
        assert_ne!(triton_recv, "0", "`triton_recv` may not be `0`");
    }

    #[rstest]
    #[tokio::test]
    async fn non_json_body_is_denied() {
        let (json_rx, app) = create_app(None);
        let request = create_webhook_request(None, String::from("foo"));
        let response = app.oneshot(request).await.unwrap();
        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        assert!(json_rx.is_empty());
    }
}
