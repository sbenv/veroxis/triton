use std::net::SocketAddr;
use std::path::PathBuf;
use std::sync::Arc;

use axum::Router;
use clap::Parser;
use color_eyre::eyre::eyre;
use color_eyre::Result;
use database::MongoDB;
use serde_json::Value;
use tokio::sync::broadcast;
use tokio::sync::broadcast::Sender;

use crate::database::{run_async_database_inserter, DynTritonDB, TritonDB};
use crate::plugins::wasm::run_async_wasm_executor;
use crate::routes::api::{api_routes, ApiState};
use crate::routes::default::default_routes;
use crate::routes::gitlab_webhook::{webhook_routes, GitlabWebhookState};
use crate::routes::health::{health_routes, ReadyzState};
use crate::scripting::lua::run_async_lua_executor;

pub mod database;
pub mod plugins;
pub mod routes;
pub mod scripting;
pub mod types;

#[derive(Debug, Parser)]
pub struct GitlabServerArgs {
    /// the port on which the gitlab-server will listen
    #[clap(long, default_value = "8080", env = "TRITON_GITLAB_SERVER_PORT")]
    pub server_port: u16,

    /// an optional secret which will then be required to send data to the "/gitlab-webhook" endpoint
    #[clap(long, env = "TRITON_GITLAB_SERVER_SECRET")]
    pub server_secret: Option<String>,

    /// an optional secret which will then be required to query data from any of the "/api/*" endpoints
    #[clap(long, env = "TRITON_GITLAB_SERVER_API_SECRET")]
    pub server_api_secret: Option<String>,

    /// the mongodb username for authentication
    #[clap(long, env = "TRITON_GITLAB_SERVER_MONGO_USER")]
    pub mongo_user: String,

    /// the mongodb password for authentication
    #[clap(long, env = "TRITON_GITLAB_SERVER_MONGO_PASS")]
    pub mongo_pass: String,

    /// the mongodb host to connect to
    #[clap(long, env = "TRITON_GITLAB_SERVER_MONGO_HOST")]
    pub mongo_host: String,

    /// the mongodb port to connect to
    #[clap(long, env = "TRITON_GITLAB_SERVER_MONGO_PORT")]
    pub mongo_port: u16,

    /// name of the database in which all collections are stored
    #[clap(
        long,
        default_value = "triton-gitlab-server",
        env = "TRITON_GITLAB_SERVER_MONGO_DATABASE_NAME"
    )]
    pub mongo_database_name: String,

    /// how long to keep entries in the "errors" collection in seconds
    #[clap(long, env = "TRITON_GITLAB_SERVER_MONGO_TTL_ERROR_LOG")]
    pub mongo_ttl_error_log: Option<u64>,

    /// how long to keep entries in the "webhook_events" collection in seconds
    #[clap(long, env = "TRITON_GITLAB_SERVER_MONGO_TTL_WEBHOOK_EVENTS")]
    pub mongo_ttl_webhook_events: Option<u64>,

    /// set this to execute the lua scripts at the given paths
    #[clap(long, env = "TRITON_GITLAB_SERVER_SCRIPTS_LUA")]
    pub scripts_lua: Option<Vec<PathBuf>>,

    /// set this to import wasm plugins in the given directories
    #[clap(long, env = "TRITON_GITLAB_SERVER_PLUGIN_DIR_WASM")]
    pub plugin_dir_wasm: Option<Vec<PathBuf>>,

    /// set this to execute the shell scripts at the given paths
    #[clap(long, env = "TRITON_GITLAB_SERVER_SCRIPTS_LUA")]
    pub scripts_shell: Vec<PathBuf>,
}

pub async fn run(args: GitlabServerArgs) -> Result<()> {
    tracing::info!("Starting server...");
    let database_client = Arc::new(
        MongoDB::new(
            &args.mongo_user,
            &args.mongo_pass,
            &args.mongo_host,
            args.mongo_port,
            &args.mongo_database_name,
            args.mongo_ttl_error_log,
            args.mongo_ttl_webhook_events,
        )
        .await?,
    );

    tracing::info!("Checking database connection...");
    database_client.ping().await?;

    tracing::info!("Initializing database...");
    database_client.init_db().await?;

    tracing::info!("Setting up broadcast channels...");
    let (json_tx, json_rx) = broadcast::channel::<Value>(255);

    let mut processes = vec![];

    if let Some(scripts_lua) = args.scripts_lua {
        tracing::info!("[lua] setting up async executor...");
        let lua_executor_handle = run_async_lua_executor(json_tx.subscribe(), scripts_lua.clone());
        processes.push(lua_executor_handle);
    }

    if let Some(plugin_dir_wasm) = args.plugin_dir_wasm {
        tracing::info!("[wasm] setting up async executor...");
        let lua_executor_handle =
            run_async_wasm_executor(json_tx.subscribe(), plugin_dir_wasm.clone());
        processes.push(lua_executor_handle);
    }

    tracing::info!("Setting up async database inserter...");
    let database_inserter_handle =
        run_async_database_inserter(json_tx.subscribe(), database_client.clone());
    processes.push(database_inserter_handle);

    drop(json_rx);

    tracing::info!("Setting up router...");
    let router = create_router(
        args.server_secret,
        args.server_api_secret,
        json_tx,
        database_client,
    );

    tracing::info!("Ready!");
    let address = SocketAddr::from(([0, 0, 0, 0], args.server_port));
    let listener = tokio::net::TcpListener::bind(&address).await?;
    let webserver: tokio::task::JoinHandle<Result<()>> = tokio::spawn(async move {
        axum::serve(listener, router.into_make_service()).await?;
        Ok(())
    });

    tokio::select! {
        webserver_result = webserver => {
            tracing::warn!("webserver finished unexpectedly: {webserver_result:?}");
            if let Err(error) = webserver_result {
                tracing::error!("{error:?}");
                return Err(error.into());
            }
        }
        processes_result = futures::future::try_join_all(processes) => {
            tracing::warn!("secondary processes finished unexpectedly: {processes_result:?}");
            if let Err(error) = processes_result {
                tracing::error!("{error:?}");
                return Err(error.into());
            }
        }
    };

    Err(eyre!("the program should not reach the end"))
}

pub fn create_router(
    server_secret: Option<String>,
    server_api_secret: Option<String>,
    json_tx: Sender<Value>,
    database_client: DynTritonDB,
) -> Router {
    Router::new()
        // health routes
        .merge(health_routes())
        .with_state(ReadyzState::new(database_client.clone()))
        // webhook routes
        .merge(webhook_routes())
        .with_state(GitlabWebhookState::new(server_secret, json_tx.clone()))
        // api routes
        .nest("/api", api_routes())
        .with_state(ApiState::new(server_api_secret, database_client, json_tx))
        // root route, 404
        .merge(default_routes())
}
