FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.1

COPY target/x86_64-unknown-linux-musl/release/triton /usr/local/bin/triton

RUN apk add --no-cache "dumb-init"

ENTRYPOINT ["dumb-init", "--"]
CMD ["triton"]
