check-all: cargo-check cargo-clippy cargo-test cargo-fmt

fix-all: cargo-fix-clippy cargo-fix-fmt

cargo-clean:
    cargo clean

cargo-check:
    cargo check

cargo-fmt:
    cargo fmt -- --check

cargo-fix-fmt:
    cargo +nightly fmt

cargo-clippy:
    cargo clippy --all-features -- -D warnings

cargo-fix-clippy:
    cargo clippy --fix --allow-dirty --allow-staged

cargo-test:
    cargo nextest run

build-release:
    cargo zigbuild --target=x86_64-unknown-linux-musl --release

build-debug:
    cargo zigbuild --target=x86_64-unknown-linux-musl

cargo-set-version version:
    cargo set-version --workspace {{version}}

nix command:
    nix --extra-experimental-features "nix-command flakes" develop --ignore-environment --command -- {{command}}
